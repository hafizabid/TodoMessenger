﻿
using Microsoft.AspNetCore.Mvc;
using TodoMessenger.ExceptionHandling;

namespace TodoMessenger.Controllers
{
    public abstract class BaseController: ControllerBase
    {
        [NonAction]
        public IActionResult ManageResponse(int statusCode, dynamic result = null)
        {
            switch (statusCode)
            {
                case StatusCodes.Status200OK:
                    if (result != null)
                    {
                        return new OkObjectResult(result);
                    }
                    else
                    {
                        return new OkResult();
                    }
                case StatusCodes.Status201Created:
                    return new CreatedAtRouteResult(new { id = result.id }, result);
                case StatusCodes.Status202Accepted:
                    return new AcceptedResult();
                case StatusCodes.Status204NoContent:
                    return new NoContentResult();
                default:
                    return new OkResult();
            }
        }

        [NonAction]
        public IActionResult ManageException(HttpCustomException httpCustomException)
        {
            return httpCustomException.StatusCode switch
            {
                StatusCodes.Status400BadRequest => new BadRequestObjectResult(httpCustomException.CustomMessage),
                StatusCodes.Status401Unauthorized => new UnauthorizedResult(),
                StatusCodes.Status403Forbidden => StatusCode(StatusCodes.Status403Forbidden, httpCustomException.CustomMessage),
                StatusCodes.Status404NotFound => new NotFoundResult(),
                _ => StatusCode(StatusCodes.Status500InternalServerError, httpCustomException.InnerException),
            };
        }
    }
}
