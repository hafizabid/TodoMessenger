using Microsoft.AspNetCore.Mvc;
using TodoMessenger.Service;
namespace TodoMessenger.Controllers;

[ApiController]
[Route("[controller]")]
public class BookManagementController : BaseController
{

    private readonly ILogger<BookManagementController> _logger;

    public BookManagementController(ILogger<BookManagementController> logger)
    {
        _logger = logger;
    }

    [HttpGet(Name = "GetBookManagement")]
    public IActionResult Get()
    {
        try
            {
                var context = new BookContext();
                return ManageResponse(StatusCodes.Status200OK, context.Book.ToList());
            }
            catch (Exception exception)
            {
                // Errorhandling non custom excepetion
                return StatusCode(StatusCodes.Status500InternalServerError, exception.ToString());
            }
    }
}
