using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using TodoMessenger.Service;
using System.Text;
using System.Text.Json;
using TodoMessenger.DBModel;
namespace TodoMessenger.Reciever;

public class ConsumeRabbitMQHostedService : BackgroundService
{
    private readonly ILogger _logger;
    private IConnection _connection;
    private IModel _channel;

    public ConsumeRabbitMQHostedService(ILoggerFactory loggerFactory)
    {
        this._logger = loggerFactory.CreateLogger<ConsumeRabbitMQHostedService>();
        InitRabbitMQ();
    }

    private void InitRabbitMQ()
    {
        var factory = new ConnectionFactory { HostName = Environment.GetEnvironmentVariable("RABBITMQ_HOST"), Port = Int32.Parse(Environment.GetEnvironmentVariable("RABBITMQ_PORT")) };
        factory.UserName = Environment.GetEnvironmentVariable("RABBITMQ_USER");
        factory.Password = Environment.GetEnvironmentVariable("RABBITMQ_PASSWORD");
        // create connection  
        _connection = factory.CreateConnection();

        // create channel  
        _channel = _connection.CreateModel();


        _channel.ExchangeDeclare("books.exchange", ExchangeType.Topic);
        _channel.QueueDeclare("books", false, false, false, null);
        _channel.QueueBind("books", "books.exchange", "books.queue.*", null);
        _channel.BasicQos(0, 1, false);

        _connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        stoppingToken.ThrowIfCancellationRequested();

        var consumer = new EventingBasicConsumer(_channel);
        consumer.Received += (ch, ea) =>
        {
            // received message  
            var content = Encoding.UTF8.GetString(ea.Body.ToArray());
            var BookRecord = JsonSerializer.Deserialize<Book>(content);

            var context = new BookContext();
            context.Add(BookRecord);
             context.SaveChanges();
            _channel.BasicAck(ea.DeliveryTag, false);
        };

        consumer.Shutdown += OnConsumerShutdown;
        consumer.Registered += OnConsumerRegistered;
        consumer.Unregistered += OnConsumerUnregistered;
        consumer.ConsumerCancelled += OnConsumerConsumerCancelled;

        _channel.BasicConsume("books", false, consumer);
        return Task.CompletedTask;
    }

    private void HandleMessage(string content)
    {
        // we just print this message   
        _logger.LogInformation($"consumer received {content}");
    }

    private void OnConsumerConsumerCancelled(object sender, ConsumerEventArgs e) { }
    private void OnConsumerUnregistered(object sender, ConsumerEventArgs e) { }
    private void OnConsumerRegistered(object sender, ConsumerEventArgs e) { }
    private void OnConsumerShutdown(object sender, ShutdownEventArgs e) { }
    private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e) { }

    public override void Dispose()
    {
        _channel.Close();
        _connection.Close();
        base.Dispose();
    }
}