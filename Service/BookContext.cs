using Microsoft.EntityFrameworkCore;
using MySql.EntityFrameworkCore.Extensions;
using TodoMessenger.DBModel;
namespace TodoMessenger.Service
{
  public class BookContext : DbContext
  {
    public DbSet<Book> Book { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.UseMySQL(Environment.GetEnvironmentVariable("MYSQL_CONNECTION_STRING"));
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);

      modelBuilder.Entity<Book>(entity =>
      {
        entity.HasKey(e => e.ISBN);
        entity.Property(e => e.Title).IsRequired();
      });
    }
  }
}